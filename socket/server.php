<?php
$errorCode = 0;

$socket = stream_socket_server("tcp://0.0.0.0:8001", $errorCode, $errorMessage);
$mysql = new mysqli("localhost", "", "", "datacollection");

$clients = array();

if($socket != null){

    while(1) {

        $client = stream_socket_accept($socket);
        if($client != null) {

            $data = stream_socket_recvfrom($client, 1024);
            $json = json_decode(rtrim($data, "\0"), false);
            var_dump($json);

            switch (strtoupper($json->type)){

                case "ACCESS":
                    fwrite($client, accessHandler($mysql, $json->name, $json->key));
                    fclose($client);
                    break;

                case "DATA":
                    fwrite($client, dataHandler($mysql, $json));
                    fclose($client);
                    break;

                default:
                    fclose($client);
                    break;
            }
        }
    }
}
echo "starting server";

//Refactor to the verify function that returns a boolean.
function accessHandler(Mysqli $mysql, $deviceName, $deviceKey): string {

    if($deviceName == null || $deviceKey == null){
        return json_encode(array("error" => true, "message" => "Missing parameters"));
    }

    $getDevice = $mysql->prepare("SELECT ID FROM Collectors WHERE Name = ? AND AccessKey = ?");
    $getDevice->bind_param("ss", $deviceName, $deviceKey);
    $getDevice->execute();
    $getDevice->store_result();

    if($getDevice->num_rows < 1){
        $getDevice->close();
        return json_encode(array("error" => true, "message" => "Access denied"));
    }
    $getDevice->bind_result($sqlid);
    $getDevice->fetch();
    $getDevice->close();

    return json_encode(array("error" => false, "message" => "Deviced were successfully verified", "data" => array("id" => $sqlid)));
}

function dataHandler(Mysqli $mysql, $data): string {

    $deviceId = $data->device->id;
    $deviceKey = $data->device->key;

    if(verify($mysql, $deviceId, $deviceKey) == false)
        return json_encode(array("error" => true, "message" => "Access denied"));


    $type = $data->collection->type;
    $roomId = $data->collection->room_id;
    $value = $data->collection->value;
    $date = time();

    $insert = $mysql->prepare("INSERT INTO Collector_Data (Collector_ID, Collector_Type_ID, Room_ID, Value, Date) VALUES(?, ?, ?, ?, ?)");
    $insert->bind_param("iiidi", $deviceId, $type, $roomId, $value, $date);
    $insert->execute();
    $insert->close();

    return json_encode(array("error" => false, "message" => "The data was successfully collected and saved."));

}

function verify(Mysqli $mysql, int $deviceId, string $deviceKey): bool {

    if($deviceId == null || $deviceKey == null)
        return false;

    $getDevice = $mysql->prepare("SELECT ID FROM Collectors WHERE ID = ? AND AccessKey = ?");
    $getDevice->bind_param("ss", $deviceId, $deviceKey);
    $getDevice->execute();
    $getDevice->store_result();

    if($getDevice->num_rows < 1) {
        $getDevice->close();
        return false;
    }

    $getDevice->close();

    return true;
}
