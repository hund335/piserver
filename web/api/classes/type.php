<?php


class type
{
    var $conn;

    public function __construct(mysqli $connection)
    {
        $this->conn = $connection;
    }

    public function exist(int $id): bool {

        $execute = $this->conn->prepare("SELECT ID FROM Collector_Types WHERE ID = ?");
        $execute->bind_param("i", $id);
        $execute->execute();
        $execute->store_result();
        $result = $execute->num_rows;
        $execute->close();

        return ($result >= 1);
    }
}