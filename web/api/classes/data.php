<?php


class data
{
    var $conn;

    public function __construct(mysqli $connection)
    {
        $this->conn = $connection;
    }

    public function fromRoom(int $id, int $type): array {
        $data = array();

        $execute = $this->conn->prepare("SELECT Collector_Data.ID, Collector_Types.ID, Collector_Types.name, Collector_Data.Value, Collector_Data.Date, Rooms.ID, Rooms.Name FROM Collector_Data 
                                               INNER JOIN Collector_Types ON Collector_Type_ID = Collector_Types.ID
                                               INNER JOIN Rooms ON Collector_Data.Room_ID = Rooms.ID WHERE Collector_Data.Room_ID = ? AND Collector_Type_ID = ? ORDER BY Collector_Data.Date DESC LIMIT 100 OFFSET 0");
        $execute->bind_param("ii", $id, $type);

        $execute->execute();
        $execute->store_result();
        $execute->bind_result($sqlid, $sqltypeid, $sqltypename, $sqldata, $sqldate, $sqlroomid, $sqlroomname);

        while($execute->fetch())
            array_push($data, array("room" => array("id" => $sqlroomid, "name" => $sqlroomname), "data" => array("type_id" => $sqltypeid, "type_name" => $sqltypename, "data" => (int)$sqldata, "date" => date('d-m-Y H:i:s', $sqldate))));

        $execute->close();

        return array("data" => $data);
    }

}