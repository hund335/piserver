<?php

class room
{
    var $conn;

    public function __construct(mysqli $connection)
    {
        $this->conn = $connection;
    }

    public function exist(int $id): bool {

        $execute = $this->conn->prepare("SELECT ID FROM Rooms WHERE ID = ?");
        $execute->bind_param("i", $id);
        $execute->execute();
        $execute->store_result();
        $result = $execute->num_rows;
        $execute->close();

        return ($result >= 1);
    }

    public function get(int $id): array {

        $execute = $this->conn->prepare("SELECT Rooms.ID,  Rooms.Name, Building_ID, Buildings.Name FROM Rooms INNER JOIN Buildings ON Rooms.Building_ID = Buildings.ID  WHERE ID = ? ORDER BY Building_ID ASC");
        $execute->bind_param("i", $id);
        $execute->execute();
        $execute->store_result();
        $execute->bind_result($sqlid, $sqlname, $sqlbuildingid, $sqlbuildingname);
        $execute->fetch();
        $execute->close();

        return array("id" => $sqlid, "name" => $sqlname, "building_id" => $sqlbuildingid, "building_name" => $sqlbuildingname);
    }

    public function getAll(){

        $rooms = array();

        $execute = $this->conn->prepare("SELECT Rooms.ID,  Rooms.Name, Building_ID, Buildings.Name FROM Rooms INNER JOIN Buildings ON Rooms.Building_ID = Buildings.ID ORDER BY Building_ID ASC ");
        $execute->execute();
        $execute->store_result();
        $execute->bind_result($sqlid, $sqlname, $sqlbuildingid, $sqlbuildingname);

        while($execute->fetch())
            array_push($rooms, array("id" => $sqlid, "name" => $sqlname, "building_id" => $sqlbuildingid, "building_name" => $sqlbuildingname));

        $execute->close();

        return array("rooms" => $rooms);
    }
}