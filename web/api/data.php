<?php
if(isset($_GET['room_id']) == null || isset($_GET['type_id']) == null){
    echo json_encode(array("status" => "error", "message" => "Please enter the id of the room, as well as the data type id"));
    return;
}
$mysql = new mysqli("localhost", "", "", "datacollection");

include "classes/room.php";
$room = new room($mysql);

$roomid = $_GET['room_id'];
$typeid = $_GET['type_id'];

if($room->exist($roomid) == false){

    $mysql->close();
    echo json_encode(array("status" => "error", "message" => "No room with that id was found"));
    return;
}

include "classes/type.php";
$type = new type($mysql);

if($type->exist($typeid) == false){

    $mysql->close();
    echo json_encode(array("status" => "error", "message" => "No type with that id exists"));
    return;
}

include "classes/data.php";
$data = new data($mysql);

echo json_encode(array("status" => "success", "message" => "Successfully", "data" => $data->fromRoom($roomid, $typeid)));

$mysql->close();

?>
