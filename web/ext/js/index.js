google.charts.load('current', {'packages':['corechart']});
//google.charts.setOnLoadCallback(generateTempChart);

function generateChart(roomid, typeid) {

    var http = new XMLHttpRequest();

    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            let items = [];
            var json = JSON.parse(http.responseText);
            for(i = 0; i < json.data.data.length; i++){

                let item = json.data.data[i];

                if(i === 0)
                    items.push(["Dato", item.data.type_name])
                else {
                    items.push([item.data.date, item.data.data]);
                    document.getElementById('data-content-title').innerText = item.room.name;
                }
            }

            var data = google.visualization.arrayToDataTable(items);

            var options = {
                title: 'Temperatur',
                hAxis: {titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: '#14af6f',
                chartArea: {height: '80%'},
                legend: {position: 'top'}
            };

            var chart = new google.visualization.AreaChart(document.getElementById('data-content'));
            chart.draw(data, options);

        }
    };
    http.open("GET", "api/data.php?room_id=" + roomid + "&type_id=" + typeid, true);
    http.send();
}

function loadRooms() {

    var http = new XMLHttpRequest();

    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var picker = document.getElementById('room-picker');

            var json = JSON.parse(http.responseText);

            for(i = 0; i < json.data.rooms.length; i++){

                var room = json.data.rooms[i];

                var pickerItem = document.createElement("option");
                pickerItem.value = room.id;
                pickerItem.innerText = room.name + "(" + room.building_name + ")";

                picker.appendChild(pickerItem);
            }

        }
    }
    http.open("GET", "api/rooms.php", true);
    http.send();
}

function optionClick() {
    var roomID = document.getElementById('room-picker').value;
    var typeID = document.getElementById('type-picker').value;

    if(roomID !== 0){
        generateChart(roomID, typeID)
    }
}